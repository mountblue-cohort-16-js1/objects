function invert(testObject) {
    if (!!testObject && typeof testObject === 'object') {
        invertedObj = {};
        for (let key in testObject) {
            invertedObj[testObject[key]] = key;
        }
        return invertedObj;
    }
    return {};
}

module.exports = invert;
