function values(testObject) {
    if (!!testObject && typeof testObject === 'object') {
        valuesArr = [];
        for (let key in testObject) {
            if (key === 'function') { continue; }
            valuesArr.push(testObject[key]);
        }
        return valuesArr;
    }
    return [];
}

module.exports = values;
