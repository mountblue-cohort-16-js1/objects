const mapObject = require("../mapObject");
const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
const cb = function (value) { return value + '_mapped'; }

console.log(mapObject(testObject, cb));
