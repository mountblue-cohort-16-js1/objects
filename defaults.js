function defaults(testObject, defaultProps) {
    if (!!testObject && !!defaultProps && typeof testObject === 'object' && typeof defaultProps == 'object') {
        for (key in defaultProps) {
            if (key in testObject) { continue; }
            else {
                testObject[key] = defaultProps[key];
            }
        return testObject;
        }
    }
    return {};
}

module.exports = defaults;
