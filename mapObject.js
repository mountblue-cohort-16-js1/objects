function mapObject(testObject, cb) {
    if (!!testObject && typeof testObject === 'object' && !!cb && typeof cb === 'function') {
        mappedObject = {};
        for (let key in testObject) {
            mappedObject[key] = cb(testObject[key]);
        }
        return mappedObject;
    }
    return {};
}

module.exports = mapObject;
