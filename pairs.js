function pairs(testObject) {
    if (!!testObject && typeof testObject === 'object') {
        pairsArr = [];
        for (let key in testObject) {
            let pairs = [key, testObject[key]];
            pairsArr.push(pairs);
        }
        return pairsArr;
    }
    return [];
}

module.exports = pairs;
