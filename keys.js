function keys(testObject) {
    if (!!testObject && typeof testObject === 'object') {
        keysArr = [];
        for (let key in testObject) {
            keysArr.push(key);
        }
        return keysArr;
    }
    return [];
}

module.exports = keys;
